
开源软件是由像你这样的人开发的。了解一下如何启动和增长您的项目。

###[![如何为开源做贡献 illustration](https://opensource.guide/assets/images/illos/contribute.svg)如何为开源做贡献想为开源贡献力量？本指南针为”菜鸟”和初学者而准备！](https://opensource.guide/zh-hans/how-to-contribute/)

###[![开启一个开源项目 illustration](https://opensource.guide/assets/images/illos/beginners.svg)开启一个开源项目从开源的世界汲取智慧，然后开始准备着手发起开源项目。](https://opensource.guide/zh-hans/starting-a-project/)

###[![为项目寻找合适的用户 illustration](https://opensource.guide/assets/images/illos/finding.svg)为项目寻找合适的用户通过找到称心如意的用户，帮助开源项目成长。](https://opensource.guide/zh-hans/finding-users/)

###[![打造受欢迎的社区 illustration](https://opensource.guide/assets/images/illos/building.svg)打造受欢迎的社区打造人们愿意使用、贡献、并主动宣传的人气社区。](https://opensource.guide/zh-hans/building-community/)

###[![维护者最佳实践 illustration](https://opensource.guide/assets/images/illos/best-practices.svg)维护者最佳实践身为开源的维护者，如何轻松驾驭项目？本指南从文档流程到有效利用社区来展开。](https://opensource.guide/zh-hans/best-practices/)

###[![领导力和治理 illustration](https://opensource.guide/assets/images/illos/leadership.svg)领导力和治理决策有了较正式的规则，可让开源项目迅速生长。](https://opensource.guide/zh-hans/leadership-and-governance/)

###[![通过为开源工作获得报酬 illustration](https://opensource.guide/assets/images/illos/getting-paid.svg)通过为开源工作获得报酬为了让你能够以开源方式维持工作，理应得到相应的经济上的报酬。](https://opensource.guide/zh-hans/getting-paid/)

###[![行为准则 illustration](https://opensource.guide/assets/images/illos/coc.svg)行为准则社区的长远发展和健康成长，离不开一些行为准则，需要遵守并执行。](https://opensource.guide/zh-hans/code-of-conduct/)

###[![开源衡量标准 illustration](https://opensource.guide/assets/images/illos/metrics.svg)开源衡量标准通过持续的追踪项目，帮助你作出最佳决策，以让开源项目更成功。](https://opensource.guide/zh-hans/metrics/)

###[![开源的法律保护 illustration](https://opensource.guide/assets/images/illos/legal.svg)开源的法律保护对于开源你应该了解的所有法律知识。](https://opensource.guide/zh-hans/legal/)